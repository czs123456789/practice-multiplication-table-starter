package com.tw;

public class MultiplicationTable {
    public String buildMultiplicationTable(int start, int end) {
        if (!isValid(start,end))
            return null;
        return generateTable(start,end);
    }

    public Boolean isValid(int start, int end) {
        return isInRange(start) && isInRange(end) && isStartNotBiggerThanEnd(start,end);
    }

    public Boolean isInRange(int number) {
        return number >=1 && number <= 1000;
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {
        return start <= end;
    }

    public String generateTable(int start, int end) {
        String result = "";
        for (int i = start;i < end;++i) {
            result += generateLine(start,i);
            result += "%n";
        }
        result += generateLine(start,end);
        return String.format(result);
    }

    public String generateLine(int start, int row) {
        String result = "";
        for (int i = start;i < row;++i) {
            result += generateSingleExpression(i, row);
            result += "  ";
        }
        result += generateSingleExpression(row,row);
        return result;
    }

    public String generateSingleExpression(int multiplicand, int multiplier) {
        int result = multiplicand*multiplier;
        return multiplicand+"*"+multiplier+"="+result;
    }
}
